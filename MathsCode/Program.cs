﻿using System;

namespace MathsCode
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 1;
            int B = 12;
            int C = 36;
            int D = A * C;
            bool Bnegative = true;
            bool Cnegative = true;
            bool UNFACTORED = false;
            int i;
            for (i = 1; i <= D; i++)
            {
                int Divisible = D % i;
                if (Divisible == 0)
                {                                                   // B+ C-
                                                                    //IF C IS NEGATIVE AND B IS POSITIVE, GIVE NEGATIVE AND POSITIVE NUMBERS
                    Console.WriteLine($"{i} is a factor of {D}");
                    int pair = D / i;
                    Console.WriteLine($"The pair is {i} and {pair}.");

                    if (i - pair == B & Cnegative == true & Bnegative == false) // B+ C-
                    {
                        Console.WriteLine($"{i}, -{pair}");
                        Console.WriteLine($"(x + {i})(x - {pair})");
                        if (UNFACTORED)
                        {
                            Console.WriteLine($"x = -{i}, x = {pair} < UNFACTORED ANSWER A");
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (i - pair == B & Cnegative == true & Bnegative == true) // B- C-
                    {
                        Console.WriteLine($"");
                        Console.WriteLine($"(x + {pair})(x - {i})");
                        if (UNFACTORED)
                        {
                            Console.WriteLine($"x = -{pair}, x = {i} < UNFACTORED ANSWER B");
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (pair - i == B & Cnegative == true & Bnegative == true) // B- C-
                    {
                        Console.WriteLine($"{i}, -{pair}");
                        Console.WriteLine($"(x + {i})(x - {pair})");
                        if (UNFACTORED)
                        {
                            Console.WriteLine($"x = -{i}, x = {pair} < UNFACTORED ANSWER C");
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (pair - i == B & Cnegative == true & Bnegative == false) // B+ C-
                    {
                        Console.WriteLine($"{pair}, -{i}");
                        Console.WriteLine($"(x + {pair})(x - {i})");
                        if (UNFACTORED)
                        {
                            Console.WriteLine($"x = -{pair}, x = {i} < UNFACTORED ANSWER D");
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (i < B)                                      // B+ C+
                                                                    //IF BOTH C AND B ARE POSITIVE, GIVE POSITIVE NUMBERS
                    {
                        int Remainder = B - i;
                        if (Remainder * i == D)
                        {
                            Console.WriteLine($"{Remainder} + {i} = {B}, {Remainder} * {i} = {D}");
                            if (Bnegative == true)
                            {
                                Console.WriteLine($"(x - {Remainder})(x - {i})"); // B- C+
                            }
                            else // Basic Nonmonic shit B+ C+
                            {
                                Console.WriteLine($"({A}x + {Remainder})({A}x + {i}) / {A}");
                                if (Remainder % A == 0)
                                {
                                    Console.WriteLine($"(x + {Remainder / A})({A}x + {i})");
                                }
                                if (i % A == 0)
                                {
                                    Console.WriteLine($"({A}x + {Remainder})(x + {i / A})");
                                }
                            }
                            
                            
                            if (Bnegative)
                            {
                                Console.WriteLine("-_-_-_-_-");
                                Console.WriteLine($"x = {Remainder}, x = {i}  UNFACTORED EQUATION SIGMA"); //used for unfactored equations if B is negative
                                Console.WriteLine("-_-_-_-_-");
                            }
                            else
                            {
                                Console.WriteLine("-_-_-_-_-");
                                Console.WriteLine($"x = -{Remainder}, x = -{i} UNFACTORED EQUATION BETA"); //used for unfactored equations if B is positive
                                Console.WriteLine("-_-_-_-_-");
                            }
                        }
                    }
                }
                    
            }
        }
    }
}
